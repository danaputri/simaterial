 @extends('adminlte::page')

@section('title', 'Tambah')

@section('content_header')
    <h1>Tambah Data</h1>
@stop

@section('content')
        <span>Tambah <strong>STOK</strong></span>
    <hr>
    <form action="{{ route('stok.store') }}" method="POST">
    	@csrf
    	<div class="form-group">
            <label for="nama_barang">Nama Barang<label>
            <input type="text" class="form-control" name="nama_barang"/>
        </div>
        <div class="form-group">
            <label for="jenis_barang">Merk Barang<label>
            <input type="text" class="form-control" name="jenis_barang"/>
        </div>
        <div class="form-group">
            <label for="jml_stok">Jumlah Stok</label>
            <input type="text" class="form-control" name="jml_stok"/>
        </div>
        <div class="form-group">
            <label for="supplier">Nama Supplier</label>
            <input type="text" class="form-control" name="supplier"/>
        </div>
        <div class="form-group">
            <label for="harga_satuan">Harga Satuan Barang</label>
            <input type="text" class="form-control" name="harga_satuan"/>
        </div>
        <div class="form-group">
            <label for="harga_total">Harga Total</label>
            <input type="text" class="form-control" name="harga_total"/>
        </div>
        <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <input type="text" class="form-control" name="keterangan"/>
        </div>
        <div class="form-group">
            <label for="tanggal">tanggal input</label>
            <input type="date" class="form-control" name="tanggal"/>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop