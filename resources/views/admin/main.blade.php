 @extends('adminlte::page')

@section('title', 'Stok material')

@section('content_header')
    <h1>Halaman Stok</h1>
@stop

@section('content')
    <div class="card show col-md-12">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold">Data Stok Material</h6>
	</div>
	<div class="card-body">
		<a href="{{ route('stok.create') }}" class="btn btn-sm btn-danger" style="float: right;">Tambah</a>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama barang</th>
					<th>Merk Barang</th>
					<th>Jumlah Barang</th>
					<th>Harga Satuan</th>
					<th>Harga Total</th>
					<th>Keterangan</th>
					<th>Tanggal</th>
					<th>Nama Supplier</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($stok as $stoks)
				<tr>
					<td>{{$stoks->id}}</td>
					<td>{{$stoks->nama_barang}}</td>
					<td>{{$stoks->jenis_barang}}</td>
					<td>{{$stoks->jml_stok}}</td>
					<td>{{number_format($stoks->harga_satuan, 2, ',', '.')}}</td>
					<td>{{number_format($stoks->harga_total, 2, ',', '.')}}</td>
					<td>{{$stoks->keterangan}}</td>
					<td>{{$stoks->tanggal}}</td>
					<td>{{$stoks->supplier}}</td>
					<td>
						<a href="{{ route('stok.edit',$stoks->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('stok.destroy', $stoks->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                		</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop