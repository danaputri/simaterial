 @extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Ubah Data</h1>
@stop

@section('content')
        <span>Ubah Data <strong>STOK</strong></span>
    <hr>
    <form action="/stok/{{$stok->id}}" method="POST">
    	@csrf
        @method('PATCH')
    	<div class="form-group">
            <label for="nama_barang">Nama Barang<label>
    <input type="text" class="form-control" name="nama_barang" value="{{$stok->nama_barang}}"/>
        </div>
        <div class="form-group">
            <label for="jenis_barang">Merk Barang<label>
            <input type="text" class="form-control" name="jenis_barang" value="{{$stok->jenis_barang}}"/>
        </div>
        <div class="form-group">
            <label for="jml_stok">Jumlah Stok</label>
            <input type="text" class="form-control" name="jml_stok" value="{{$stok->jml_stok}}"/>
        </div>
        <div class="form-group">
            <label for="supplier">Nama Supplier</label>
            <input type="text" class="form-control" name="supplier" value="{{$stok->supplier}}"/>
        </div>
        <div class="form-group">
            <label for="harga_satuan">Harga Satuan Barang</label>
            <input type="text" class="form-control" name="harga_satuan" value="{{$stok->harga_satuan}}"/>
        </div>
        <div class="form-group">
            <label for="harga_total">Harga Total</label>
            <input type="text" class="form-control" name="harga_total" value="{{$stok->harga_total}}"/>
        </div>
        <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <input type="text" class="form-control" name="keterangan" value="{{$stok->keterangan}}"/>
        </div>
        <div class="form-group">
            <label for="tanggal">tanggal input</label>
            <input type="date" class="form-control" name="tanggal" value="{{$stok->tanggal}}" />
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop