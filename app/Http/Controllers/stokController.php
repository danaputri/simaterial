<?php

namespace App\Http\Controllers;
use App\Stok;
use Illuminate\Http\Request;


class stokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stok = Stok::all();
        return view('admin.main', compact('stok'));
        // return view('admin/main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stok = new Stok();
        $stok->nama_barang = request('nama_barang');
        $stok->jenis_barang = request('jenis_barang');
        $stok->jml_stok = request('jml_stok');
        $stok->supplier = request('supplier');
        $stok->harga_satuan = request('harga_satuan');
        $stok->harga_total = request('harga_total');
        $stok->keterangan = request('keterangan');
        $stok->tanggal = request('tanggal');
        $stok->save();

        return redirect('stok')->with('success','stok sudah ditambahkan ke database');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stok = Stok::findOrFail($id);
         return view('admin.edit',compact('stok'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stok = Stok::findOrFail($id);
        $stok->nama_barang = request('nama_barang');
        $stok->jenis_barang = request('jenis_barang');
        $stok->jml_stok = request('jml_stok');
        $stok->supplier = request('supplier');
        $stok->harga_satuan = request('harga_satuan');
        $stok->harga_total = request('harga_total');
        $stok->keterangan = request('keterangan');
        $stok->tanggal = request('tanggal');
        $stok->save();
        return redirect('stok')->with('success','database stok telah berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stok = Stok::findOrFail($id);
        $stok->delete();

        return redirect('stok')->with('success', 'data stok is successfully deleted');
    }
}
