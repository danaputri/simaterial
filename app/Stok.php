<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $fillabel = ['nama_barang', 'jenis_barang', 'jml_stok', 'supplier', 'harga_satuan', 'harga_total', 'keterangan', 'tanggal'];
}
